########################################################################################################################################################################################################

LOG 0 (JANUARY 12 2019)

- All board designs (controller and buzzer) are sized to fit the version 2 casings (Arbie's Design)
- Specific sizes:
	- Controller:		3 in x 4 in
	- Buzzer:			1 in x 1.5 in
- Use of female pinheads are employed for wire jumpers to connect the board and off-board components like jacks, ports, and switches
- Registered jacks (RJs) other than RJ22 may be used as replacement as long as such RJ can accommodate at least 4 separate wires
- The microcontroller used is PIC16F877A (40 pins)
- Replacement within the PIC16F87XA may be used
- The 3904 NPN transistors may be replaced by other NPN transistors
- The collector and base resistors should be changed appropriately such that the collector current is ~40-50 mA (~5-7 mA per LED branch)
- The external clock may not be 8 MHz, just adjust the code so that the output sounds will still be correct
- Power regulators may be replaced given that the replacements will still supply ~5 V (for 7805) and ~9 V (for 7809) and the pin mappings stay the same
- This design assumes a supply of 12 V similar to Arbie's design but replacements of power regulators may require different input
- For any replacement, NEVER FORGET TO ADJUST THE NECESSARY RESISTOR AND CAPACITOR VALUES
- Also, ENSURE THAT THE MICROCONTROLLER'S MINIMUM AND MAXIMUM ELECTRICAL CHARACTERISTICS ARE NOT VIOLATED
- ALWAYS CHECK DATASHEETS

- buzzer_v3
	- compatible to controller_v2 (Arbie's Design) assuming RJ22 wire mappings match
	- IN port of buzzer is OUT port of controller
	- IN port is connected to the collector of 3904 NPN transistor inside the controller
	- OUT port of buzzer is IN port of controller
	- OUT port is connected to external pull-up inside the controller

- controller_v3
	- compatible to buzzer_v2 (Arbie's Design) assuming RJ22 wire mappings match
	- most pins of PIC16F877A are utilized to extend controller capability up to 10 BUZZERS
	- has ON-BOARD PROGRAMMING PINS to be connected to a programmer/debugger kit/tool (use of PICKit of Microchip is recommended)
	- has ON-BOARD and OFF-BOARD RESET switches. The former will be used for debugging purposes while the latter is for actual application
	- has 5V and 9V notification LEDs for troubleshooting purposes (can be of any color; use of different colored LEDs is recommended)
	- has POWER ON LED to indicate ON/OFF state
	- PINS [21:30] are input pins of microcontroller (controller IN port)
	- they are held by external pull-ups with 10k resistors
	- to be specific, they are the ports RC[4:7] and RD[2:7]
	- PINS [7:10] and [15:20] are output pins of microcontroller (drives the controller OUT port)
	- they are connected to the base of 3904 NPN transitor via 1k resistors
	- to be specific, they are the ports RA5, RE[0:2], RC[0:3], and RD[0:1]
	- the buzzers sounds are produced at PIN 4 (RA2)
	- the microcontroller uses external crystal clock of 8 MHz connected at PINS [13:14] (CLKI:CLKO)

END OF LOG 0

########################################################################################################################################################################################################